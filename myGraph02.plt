set output "myGraph02.png"
set title "myGraph02"
set xlabel "Time (second)"
set ylabel "Key material (bit)"

set border linewidth 2
set terminal pngcair size 1524,768 enhanced font 'Helvetica,18'
set yrange[1:1194336];
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 3 linecolor rgb 'black' linetype 1 linewidth 1
set style line 4 linecolor rgb "#8A2BE2" linetype 1 linewidth 1
set style line 5 linecolor rgb 'blue' linetype 8 linewidth 1
set style line 6 linecolor rgb 'blue' linetype 10 linewidth 1
set style line 7 linecolor rgb 'gray' linetype 0 linewidth 1
set style line 8 linecolor rgb 'gray' linetype 0 linewidth 1 
set grid ytics lt 0 lw 1 lc rgb "#ccc"
set grid xtics lt 0 lw 1 lc rgb "#ccc"
set mxtics 5
set grid mxtics xtics ls 7, ls 8
set arrow from graph 1,0 to graph 1.03,0 size screen 0.025,15,60 filled ls 3
set arrow from graph 0,1 to graph 0,1.03 size screen 0.025,15,60 filled ls 3
set style fill transparent solid 0.4 noborder
set key outside
set key right bottom
plot "myGraph02_data.dat" index 0 title "Amount of key material" with linespoints  ls 1, "myGraph02_data.dat" index 1 title "Threshold" with linespoints  ls 4, "myGraph02_data.dat" index 2 title "Minimum" with linespoints  ls 5, "myGraph02_data.dat" index 3 title "Maximum" with linespoints  ls 6, "myGraph02_data.dat" index 4 title "Status: READY" with filledcurve  y1=0 lc rgb '#b0fbb4', "myGraph02_data.dat" index 5 title "Status: WARNING" with filledcurve  y1=0 lc rgb '#f7ea50', "myGraph02_data.dat" index 6 title "Status: CHARGING" with filledcurve  y1=0 lc rgb '#fbc4ca', "myGraph02_data.dat" index 7 title "Status: EMPTY" with filledcurve  y1=0 lc rgb '#e6e4e4'
