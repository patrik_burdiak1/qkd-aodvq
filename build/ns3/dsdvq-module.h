
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_DSDVQ
    

// Module headers:
#include "dsdvq-helper.h"
#include "dsdvq-packet-queue.h"
#include "dsdvq-packet.h"
#include "dsdvq-routing-protocol.h"
#include "dsdvq-rtable.h"
#endif
