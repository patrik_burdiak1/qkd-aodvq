
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_AODVQ
    

// Module headers:
#include "aodvq-dpd.h"
#include "aodvq-helper.h"
#include "aodvq-id-cache.h"
#include "aodvq-neighbor.h"
#include "aodvq-packet.h"
#include "aodvq-routing-protocol.h"
#include "aodvq-rqueue.h"
#include "aodvq-rtable.h"
#endif
